
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Eric's .vimrc File
"
" Installing:
" 1. cd .vim/bundle
" 2. git clone 'https://github.com/gmarik/Vundle.vim.git'
" 3. vim
" 5. :PluginInstall
"
" Links:
" http://vimawesome.com/
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== General 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set history=700         " Set no. of lines vim remembers
set autoread            " Read when file changed outside
syntax on               " syntax highlighting
set wildmenu            " Wild menu engage
set lazyredraw
set so=7
set ruler
set laststatus=3        " Show status line

" Format status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

" Ignore back files
set nobackup
set nowb
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Space settings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ts=4 sw=4 et		" A tab is 4 spaces
set softtabstop=4		" An indent is 4 spaces
set shiftround			" Round spaces to nearest shiftwidth multiple
set nojoinspaces		" Don't convert spaces to tabs
set tw=80


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Movement settings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Return to last edit position when opening files
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g'\"" |
    \ endif

" Remember info about open buffers on close
set viminfo^=%


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Movement settings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Go to shell = Ctrl-D
noremap <C-d> :sh<cr>

" Fast saving = \w
nmap <leader>w :w!<cr>  


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Pasting Function
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" If paste mode enabled, return true
function! HasPaste()
    if &paste
        return 'PASTE MODE '
    en
    return ''
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Language Handle
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Make
autocmd FileType make setlocal noet 

" Python
autocmd FileType python setlocal noet


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Tex File Handle
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_MultipleCompileFormats = 'pdf, aux'


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Environment Handler (Optional)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

colorscheme desert


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Vundle plugin manager
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" updating
" :source %
" PluginInstall

set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Essential Plugins
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-surround'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'mileszs/ack.vim'

" Language and Frameworks
Plugin 'tpope/vim-rails'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'digitaltoad/vim-pug'
" Plugin 'OmniSharp/omnisharp-vim'

" Completion
" Plugin 'Valloric/YouCompleteMe'

" Interface
Plugin 'bling/vim-airline'

" Display
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'nanotech/jellybeans.vim'

call vundle#end()
filetype plugin indent on


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Basic Plugin Modifications 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" eslint
let g:syntastic_javascript_checkers = ['eslint']

" vim-indent-guides
au VimEnter * :IndentGuidesEnable


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ============== Omnisharp Modifications 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"This is the default value, setting it isn't actually necessary
"let g:OmniSharp_host = "http://localhost:2000"

"Set the type lookup function to use the preview window instead of the status line
"let g:OmniSharp_typeLookupInPreview = 1

"Timeout in seconds to wait for a response from the server
"let g:OmniSharp_timeout = 1

"Showmatch significantly slows down omnicomplete
"when the first match contains parentheses.
"set noshowmatch

"Super tab settings - uncomment the next 4 lines
"let g:SuperTabDefaultCompletionType = 'context'
"let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
"let g:SuperTabDefaultCompletionTypeDiscovery = ["&omnifunc:<c-x><c-o>","&completefunc:<c-x><c-n>"]
"let g:SuperTabClosePreviewOnPopupClose = 1

"don't autoselect first item in omnicomplete, show if only one item (for preview)
"remove preview if you don't want to see any documentation whatsoever.
"set completeopt=longest,menuone,preview
" Fetch full documentation during omnicomplete requests.
" There is a performance penalty with this (especially on Mono)
" By default, only Type/Method signatures are fetched. Full documentation can still be fetched when
" you need it with the :OmniSharpDocumentation command.
" let g:omnicomplete_fetch_documentation=1

"Move the preview window (code documentation) to the bottom of the screen, so it doesn't move the code!
"You might also want to look at the echodoc plugin
"set splitbelow

" Get Code Issues and syntax errors
"let g:syntastic_cs_checkers = ['syntax', 'semantic', 'issues']
" If you are using the omnisharp-roslyn backend, use the following
" let g:syntastic_cs_checkers = ['code_checker']
"augroup omnisharp_commands
"    autocmd!

    "Set autocomplete function to OmniSharp (if not using YouCompleteMe completion plugin)
    "autocmd FileType cs setlocal omnifunc=OmniSharp#Complete

    " Synchronous build (blocks Vim)
    "autocmd FileType cs nnoremap <F5> :wa!<cr>:OmniSharpBuild<cr>
    " Builds can also run asynchronously with vim-dispatch installed
    "autocmd FileType cs nnoremap <leader>b :wa!<cr>:OmniSharpBuildAsync<cr>
    " automatic syntax check on events (TextChanged requires Vim 7.4)
    "autocmd BufEnter,TextChanged,InsertLeave *.cs SyntasticCheck

    " Automatically add new cs files to the nearest project on save
    "autocmd BufWritePost *.cs call OmniSharp#AddToProject()

    "show type information automatically when the cursor stops moving
    "autocmd CursorHold *.cs call OmniSharp#TypeLookupWithoutDocumentation()

    "The following commands are contextual, based on the current cursor position.

    "autocmd FileType cs nnoremap gd :OmniSharpGotoDefinition<cr>
    "autocmd FileType cs nnoremap <leader>fi :OmniSharpFindImplementations<cr>
    "autocmd FileType cs nnoremap <leader>ft :OmniSharpFindType<cr>
    "autocmd FileType cs nnoremap <leader>fs :OmniSharpFindSymbol<cr>
    "autocmd FileType cs nnoremap <leader>fu :OmniSharpFindUsages<cr>
    "finds members in the current buffer
    "autocmd FileType cs nnoremap <leader>fm :OmniSharpFindMembers<cr>
    " cursor can be anywhere on the line containing an issue
    "autocmd FileType cs nnoremap <leader>x  :OmniSharpFixIssue<cr>
    "autocmd FileType cs nnoremap <leader>fx :OmniSharpFixUsings<cr>
    "autocmd FileType cs nnoremap <leader>tt :OmniSharpTypeLookup<cr>
    "autocmd FileType cs nnoremap <leader>dc :OmniSharpDocumentation<cr>
    "navigate up by method/property/field
    "autocmd FileType cs nnoremap <C-K> :OmniSharpNavigateUp<cr>
    "navigate down by method/property/field
    "autocmd FileType cs nnoremap <C-J> :OmniSharpNavigateDown<cr>

"augroup END


" this setting controls how long to wait (in ms) before fetching type / symbol information.
"set updatetime=500
" Remove 'Press Enter to continue' message when type information is longer than one line.
"set cmdheight=2

" Contextual code actions (requires CtrlP or unite.vim)
"nnoremap <leader><space> :OmniSharpGetCodeActions<cr>
" Run code actions with text selected in visual mode to extract method
"vnoremap <leader><space> :call OmniSharp#GetCodeActions('visual')<cr>

" rename with dialog
"nnoremap <leader>nm :OmniSharpRename<cr>
"nnoremap <F2> :OmniSharpRename<cr>
" rename without dialog - with cursor on the symbol to rename... ':Rename newname'
"command! -nargs=1 Rename :call OmniSharp#RenameTo("<args>")

" Force OmniSharp to reload the solution. Useful when switching branches etc.
"nnoremap <leader>rl :OmniSharpReloadSolution<cr>
"nnoremap <leader>cf :OmniSharpCodeFormat<cr>
" Load the current .cs file to the nearest project
"nnoremap <leader>tp :OmniSharpAddToProject<cr>

" (Experimental - uses vim-dispatch or vimproc plugin) - Start the omnisharp server for the current solution
"nnoremap <leader>ss :OmniSharpStartServer<cr>
"nnoremap <leader>sp :OmniSharpStopServer<cr>

" Add syntax highlighting for types and interfaces
"nnoremap <leader>th :OmniSharpHighlightTypes<cr>
"Don't ask to save when changing buffers (i.e. when jumping to a type definition)
"set hidden
