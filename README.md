## Hello
Personal config files for vim, ssh, and bash

## Vim
.vimrc: Place at home directory ~/

## Bash
.bashrc: Place at home directory ~/

## Ssh
config: Place at ~/.ssh/

## Terminal Commands
function mygrep { grep -rnIioE --exclude-dir=node_modules ".{0,20}$1.{0,20}" . --color; }
